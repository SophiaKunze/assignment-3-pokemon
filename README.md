# Pokemon - an online Pokemon trainer with Angular

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/Readme-Standard-green?logo=markdown)](https://github.com/RichardLitt/standard-readme)
[![Angular](https://img.shields.io/badge/-Angular-black?logo=angular)](https://angular.io/)
[![JavaScript](https://img.shields.io/badge/-JavaScript-black?logo=javascript)](https://developer.mozilla.org/en-US/docs/web/javascript)
[![TailwindCss](https://img.shields.io/badge/-TailwindCSS-black?logo=tailwindcss)](https://tailwindcss.com/)

This repository contains:

1. [pokemon-components-tree.pdf](pokemon-components-tree.pdf) displaying components structure.
2. [package-lock.json](package-lock.json) listing versions of installed packages
3. [package.json](package.json) listing metadata, dependencies and scripts
4. [src](src) containing the source code, environment files and images

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background
This app is an online Pokemon trainer. It is an assignment in the scope of a fullstack development course by [Noroff](https://www.noroff.no/en/). A user can login, catch and release Pokemon by clicking on Pokeball. All Pokemon that can be caught or released are displayed in the 'catalogue page'. The 'trainer page' displays all Pokemon that were caught by the user. Username and caught Pokemon are stored in an api. Therefore, the user can still access caught Pokemon after logging out and logging in again.

## Install
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.
Clone repository via `git clone` and install dependencies via `npm install`. 

## Usage
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files. Alternatively, visit this application hosted on [Heroku](https://sophias-pokemon-app.herokuapp.com/).

## Maintainers
[@SophiaKunze](https://gitlab.com/SophiaKunze).

## Contributing
Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-3-pokemon/-/issues/new). This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct. Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Acknowledgement
This project exists thanks to my teacher <a href="https://gitlab.com/sumodevelopment">Dewald Els</a> and <a href=https://www.experis.de/de>Experis</a>.

## License
[MIT](https://opensource.org/licenses/MIT) © Sophia Kunze
