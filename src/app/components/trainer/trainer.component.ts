import { Component, OnInit } from '@angular/core';
import { SinglePokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { CatalogueService } from 'src/app/services/catalogue.service';


@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  // if something about the trainer changes, page will be notified about the change
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
    }

  get pokemonWithImage(): SinglePokemon[] | []{
    // return caught pokemons with images
    if (this.trainerService.trainer && this.trainerService.trainer.pokemon.length > 0) {
      const pokemonWithImage = this.trainerService.trainer.pokemon.map<any>(singleCaughtPokemon => {
        return this.catalogueService.pokemon?.find(singleCollectionPokemon => singleCollectionPokemon.name === singleCaughtPokemon)
      })
      return pokemonWithImage
    }
    
    return [];
  }

  constructor(
    private trainerService: TrainerService,
    private catalogueService: CatalogueService
  ) { }

  ngOnInit(): void {
  }

} 
