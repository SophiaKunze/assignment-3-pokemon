import { Component, OnInit } from '@angular/core';
import { SinglePokemon } from 'src/app/models/pokemon.model';
import { CatalogueService } from 'src/app/services/catalogue.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.scss'],
})
export class CataloguePage implements OnInit {
  get pokemon(): SinglePokemon[] {
    return this.catalogueService.pokemon!;
  }

  get loading(): boolean {
    return this.catalogueService.loading;
  }

  get error(): string {
    return this.catalogueService.error;
  }

  constructor(private readonly catalogueService: CatalogueService) {}

  ngOnInit(): void {
    this.catalogueService.findAllPokemon();
  }
}
