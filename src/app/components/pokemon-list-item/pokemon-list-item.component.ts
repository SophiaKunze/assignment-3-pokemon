import { Component, Input, OnInit } from '@angular/core';
import { SinglePokemon } from 'src/app/models/pokemon.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.scss']
})
export class PokemonListItemComponent implements OnInit {

  public isCaught: boolean = false;

  @Input() singlePokemon!: SinglePokemon;

  constructor(private trainerService: TrainerService) { }

  ngOnInit(): void {
    this.isCaught = this.trainerService.inCollection(this.singlePokemon.name);
  }
}
