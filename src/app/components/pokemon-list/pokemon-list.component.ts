import { Component, Input, OnInit } from '@angular/core';
import { SinglePokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  @Input() pokemon: SinglePokemon[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
