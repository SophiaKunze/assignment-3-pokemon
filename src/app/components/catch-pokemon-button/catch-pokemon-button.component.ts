import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.scss'],
})
export class CatchPokemonButtonComponent implements OnInit {
  
  public loading: boolean = false;
  // public isCaught: boolean = false;
  
  @Input() pokemonName: string = '';
  @Output() isCaught: boolean = false;

  constructor(
    private trainerService: TrainerService,
    private readonly catchService: CatchService
    ) {}

  ngOnInit(): void {
  // inputs are resolved
    this.isCaught = this.trainerService.inCollection(this.pokemonName);
  }

  onCatch(): void {
    this.loading = true;
    // add pokemon to trainer's collection
    this.catchService.addToCollection(this.pokemonName).subscribe({
      next: (response: Trainer) => {
        this.loading = false
        this.isCaught = this.trainerService.inCollection(this.pokemonName);
      },
      error: (error: HttpErrorResponse) => {
        console.log('onCatch:', error.message);
      },
    });
  }
}
