import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainersUrl, apiTrainersKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  // dependency injection
  // private to avoid component to have direct access to http client
  // readonly because we will never re-instantiate it
  constructor(private readonly http: HttpClient) {}

  
  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username).pipe(
      // switchMap allows to switch to a different observable
      switchMap((trainer: Trainer | undefined) => {
        // trainer does not exist
        if (trainer === undefined) {
          return this.createTrainer(username);
        }
        // if trainer already exists
        return of(trainer);
      })
    )
  }
  // check if trainer exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http
      .get<Trainer[]>(`${apiTrainersUrl}?username=${username}`)
      .pipe(
        // map is an rxjs
        map((response: Trainer[]) => {
          // response.pop takes last item on array and return it back into map and return value will be single trainer
          // if array is empty, undefined will be returned
          return response.pop();
        }) 
      );
  }

  // create trainer
  private createTrainer(username: string): Observable<Trainer> {
    // trainer
    const trainer = {
      username,
      pokemon: [],
    };
    // headers storing api key
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiTrainersKey,
    });
    // POST request for updating with new trainer
    return this.http.post<Trainer>(apiTrainersUrl, trainer, { headers });
  } 
}
