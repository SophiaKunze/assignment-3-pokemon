import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ImageUrls } from '../enums/image-urls.enum';
import { StorageKeys } from '../enums/storage-keys.enum';
import { SinglePokemon, PokemonResponse } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.utils';

const { apiPokemonUrl } = environment;
const imageBaseUrl= ImageUrls.BaseUrl;

@Injectable({
  providedIn: 'root',
})
export class CatalogueService {
  private _pokemon?: SinglePokemon[];
  private _error: string = '';
  private _loading: boolean = false;

  get pokemon(): SinglePokemon[] | undefined {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  set pokemon(pokemon: SinglePokemon[] | undefined) {
    StorageUtil.storageSave<SinglePokemon[]>(StorageKeys.Pokemon, pokemon!);
    this._pokemon = pokemon;
  }

  constructor(private readonly http: HttpClient) {
    // this._pokemon is undefined in storage, if pokemon does not exist, otherwise returns pokemon 
    this._pokemon = StorageUtil.storageRead<SinglePokemon[]>(StorageKeys.Pokemon);
  }

  private getImageUrl(url: string): string {
    // access truthy values only, empty strings are stripped out and pop last item
    const id = Number(url.split('/').filter(Boolean).pop());
    return `${imageBaseUrl}/${id}.svg`
  }

  public findAllPokemon(): void {
    
    // avoid fetching pokemons if already fetched or are fetching
    if (this.pokemon && (this._pokemon!.length > 0 || this.loading)) {
      return 
    }
    
    this._loading = true;
    this.http
      // get returns an observable: stream of data that can change over time
      // returned type of get is generic, in this case PokemonResponse
      .get<PokemonResponse>(apiPokemonUrl) 
      .pipe(
        // finalize runs after everything else is done
        finalize(() => {
          this._loading = false;
        })
      )
      // subscriber observes stream of data
      .subscribe({
        // next is equivalent to .then of a promise
        next: (response: PokemonResponse) => {
          this.pokemon = response.results.map(pokemon => {
            return {
              // image url is part of the pokemon object
              ...pokemon,
              image: this.getImageUrl(pokemon.url)
            }
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
      });
  }
}

