export class StorageUtil {
  // IMPORTANT NOTE: in production level application
  // usually would get a json web token that can be stored in a cookie or in memory
  // this web token would be sent to the server and will then identify the user that is logged in
  public static storageSave<T>(key: string, value: T): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  public static storageRead<T>(key: string): T | undefined {
    const storedValue = sessionStorage.getItem(key);
    try {
      if (storedValue) {
        return JSON.parse(storedValue) as T;
      }
      return undefined;
    } catch (error) {
      // if it's invalid json, it should be removed from storage
      sessionStorage.removeItem(key);
      return undefined;
    }
  }
}
