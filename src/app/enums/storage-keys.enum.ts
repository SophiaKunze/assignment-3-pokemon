export enum StorageKeys {
    Trainer = "pokemon-trainer",
    Pokemon = "pokemon-catalogue"
}