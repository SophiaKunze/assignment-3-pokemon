import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  // ? defines of given type or undefined
  private _trainer?: Trainer;

  // getter method for trainer
  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  // setter method for trainer
  set trainer(trainer: Trainer | undefined) {
    // trainer! -> ! means that trainer will never be undefined
    // in production level app user would be stored for example as cookie
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    // this._trainer is undefined in storage, if trainer does not exist, otherwise returns trainer
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  public inCollection(pokemonName: string): boolean {
    if (this._trainer) {
      return Boolean(
        this._trainer?.pokemon.find(
          (pokemon: string) => pokemon === pokemonName
        )
      );
    }
    return false;
  }

  public removeFromCollection(removedPokemonName: string): void {
    if (this._trainer) {
      // remove pokemon from collection
      this._trainer.pokemon = this._trainer.pokemon.filter(
        (pokemonName: string) => removedPokemonName !== pokemonName
      );
    }
  }

  public addToCollection(addedPokemonName: string): void {
    if (this._trainer) {
      // add pokemon to collection
      this._trainer.pokemon.push(addedPokemonName);
    }
  }
}
