import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { CatalogueService } from './catalogue.service';
import { TrainerService } from './trainer.service';

const { apiTrainersKey, apiTrainersUrl } = environment;

@Injectable({
  providedIn: 'root',
})
export class CatchService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: CatalogueService,
    private readonly trainerService: TrainerService
  ) {}

  public addToCollection(pokemonName: string): Observable<Trainer> {
    /** validation */
    if (!this.trainerService.trainer) {
      throw new Error('addToCollection: There is no trainer!');
    }
    const trainer: Trainer = this.trainerService.trainer;

    // if name of pokemon does not exist (undefined, null or empty string)
    if (!pokemonName) {
      throw new Error('addToCollection: Invalid pokemon name!');
    }

    // toggle pokemon collection
    if (this.trainerService.inCollection(pokemonName)) {
      this.trainerService.removeFromCollection(pokemonName);
    }
    else {
      this.trainerService.addToCollection(pokemonName);
    }

    /** patch request with trainerId and pokemon name */
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiTrainersKey,
    });

    return this.http
      .patch<Trainer>(
        `${apiTrainersUrl}/${trainer.id}`,
        {
          pokemon: [...trainer.pokemon], // patch updated array
        },
        {
          headers,
        }
      )
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer; // trainer setter makes sure that trainer is updated in session storage
        }),
      );
  }
}
